-- ----------------------------------------------------------------------------
-- Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
-- 2017-10-27
--
-- boissezon.remy@gmail.com
-- valentin@prodhomme.me
-- chill3d@protonmail.com
-- alexandre.zanni@engineer.com
--
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software.  You can  use,
-- modify and/ or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and INRIA at the following URL
-- "http://www.cecill.info".
--
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ----------------------------------------------------------------------------

-- ----------------------------------------------------------------------------
-- Database {{DATA_DB_NAME}}
-- ----------------------------------------------------------------------------
CREATE DATABASE IF NOT EXISTS `{{DATA_DB_NAME}}`
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_unicode_ci;

USE `{{DATA_DB_NAME}}`;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- Users
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- User `u_canvas_product_inventory`
CREATE USER `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`
  IDENTIFIED BY "{{DATA_U_CANVAS_PRODUCT_INVENTORY_PASSWORD}}";

-- User `u_canvas_vulnerability_inventory`
CREATE USER `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`
  IDENTIFIED BY "{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_PASSWORD}}";

-- User `u_canvas_asset_inventory`
CREATE USER `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`
  IDENTIFIED BY "{{DATA_U_CANVAS_ASSET_INVENTORY_PASSWORD}}";

-- User `u_canvas_dashboard`
CREATE USER `u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
  IDENTIFIED BY "{{DATA_U_CANVAS_DASHBOARD_PASSWORD}}";

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- Tables
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- ----------------------------------------------------------------------------
-- WFN related tables
-- ----------------------------------------------------------------------------
-- Table `{{DATA_DB_NAME}}`.`t_wfn_item`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_wfn_item` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `digest` BINARY(20) NOT NULL,
  `part` ENUM('a','o','h') NOT NULL,
  `vendor` TINYTEXT NOT NULL,
  `product` TINYTEXT NOT NULL,
  `version` TINYTEXT NOT NULL,
  `update` TINYTEXT NOT NULL,
  `edition` TINYTEXT NOT NULL,
  `sw_edition` TINYTEXT NOT NULL,
  `target_sw` TINYTEXT NOT NULL,
  `target_hw` TINYTEXT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uk_digest` (`digest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------------------------------------------------------
-- CPE `t_cpe_` related tables
-- ----------------------------------------------------------------------------
-- Table `{{DATA_DB_NAME}}`.`t_cpe_generator`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cpe_generator` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `process_start_date` DATETIME NOT NULL,
  `process_end_date` DATETIME DEFAULT NULL,
  `cpe_data_timestamp` DATETIME NOT NULL,
  `cpe_schema_version` TINYTEXT NOT NULL,
  `cpe_product_name` TINYTEXT NULL,
  `cpe_product_version` TINYTEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cpe_item`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cpe_item` (
  `wfn` INT UNSIGNED NOT NULL,
  `deprecated` BOOLEAN NOT NULL,
  `deprecation_date` DATETIME NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`wfn`),
  INDEX `fk_cpe_has_wfn` (`wfn` ASC),
  CONSTRAINT `fk_cpe_has_wfn`
    FOREIGN KEY (`wfn`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_wfn_item` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  INDEX `fk_cpe_has_generator` (`generator` ASC),
  CONSTRAINT `fk_cpe_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cpe_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cpe_item_title`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cpe_item_title` (
  `cpe` INT UNSIGNED NOT NULL,
  `digest` BINARY(20) NOT NULL,
  `lang` NCHAR(5) NOT NULL,
  `value` MEDIUMTEXT NOT NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`cpe`, `digest`),
  INDEX `fk_cpe_title_has_cpe` (`cpe` ASC),
  CONSTRAINT `fk_cpe_title_has_cpe`
    FOREIGN KEY (`cpe`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cpe_item` (`wfn`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `fk_cpe_title_has_generator` (`generator` ASC),
  CONSTRAINT `fk_cpe_title_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cpe_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cpe_item_reference`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cpe_item_reference` (
  `cpe` INT UNSIGNED NOT NULL,
  `digest` BINARY(20) NOT NULL,
  `href` VARCHAR(2083) NOT NULL,
  `value` MEDIUMTEXT NOT NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`cpe`, `digest`),
  INDEX `fk_cpe_reference_has_cpe` (`cpe` ASC),
  CONSTRAINT `fk_cpe_reference_has_cpe`
    FOREIGN KEY (`cpe`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cpe_item` (`wfn`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `fk_cpe_reference_has_generator` (`generator` ASC),
  CONSTRAINT `fk_cpe_reference_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cpe_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cpe_item_deprecated_by`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cpe_item_deprecated_by` (
  `cpe` INT UNSIGNED NOT NULL,
  `digest` BINARY(20) NOT NULL,
  `wfn` INT UNSIGNED NOT NULL,
  `date` DATETIME NOT NULL,
  `type` TINYTEXT NOT NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`cpe`, `digest`),
  INDEX `fk_cpe_deprecated_by_has_cpe` (`cpe` ASC),
  CONSTRAINT `fk_cpe_deprecated_by_has_cpe`
    FOREIGN KEY (`cpe`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cpe_item` (`wfn`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `fk_cpe_deprecated_by_has_wfn` (`wfn` ASC),
  CONSTRAINT `fk_cpe_deprecated_by_has_wfn`
    FOREIGN KEY (`wfn`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_wfn_item` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  INDEX `fk_cpe_deprecated_by_has_generator` (`generator` ASC),
  CONSTRAINT `fk_cpe_deprecated_by_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cpe_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------------------------------------------------------
-- CVE `t_cve_` related tables
-- ----------------------------------------------------------------------------
-- Table `{{DATA_DB_NAME}}`.`t_cve_generator`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cve_generator` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `process_start_date` DATETIME NOT NULL,
  `process_end_date` DATETIME DEFAULT NULL,
  `cve_data_timestamp` DATETIME NOT NULL,
  `cve_data_type` VARCHAR(16) NULL,
  `cve_data_format` VARCHAR(16) NULL,
  `cve_data_version` VARCHAR(16) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cve_item`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cve_item` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `year` NCHAR(4) NOT NULL,
  `digits` VARCHAR(32) NOT NULL,
  `published_date` DATETIME NOT NULL,
  `modified_date` DATETIME NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uk_cve_id` (`year`,`digits`),
  INDEX `fk_cve_has_generator` (`generator` ASC),
  CONSTRAINT `fk_cve_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cve_item_description`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cve_item_description` (
  `cve` INT UNSIGNED NOT NULL,
  `digest` BINARY(20) NOT NULL,
  `lang` NCHAR(5) NOT NULL,
  `value` MEDIUMTEXT NOT NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`cve`,`digest`),
  INDEX `fk_cve_description_has_cve` (`cve` ASC),
  CONSTRAINT `fk_cve_description_has_cve`
    FOREIGN KEY (`cve`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `fk_cve_description_has_generator` (`generator` ASC),
  CONSTRAINT `fk_description_cve_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cve_item_reference`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cve_item_reference` (
  `cve` INT UNSIGNED NOT NULL,
  `digest` BINARY(20) NOT NULL,
  `url` VARCHAR(2083) NOT NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`cve`,`digest`),
  INDEX `fk_cve_reference_has_cve` (`cve` ASC),
  CONSTRAINT `fk_cve_reference_has_cve`
    FOREIGN KEY (`cve`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `fk_cve_reference_has_generator` (`generator` ASC),
  CONSTRAINT `fk_cve_reference_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cve_item_affect`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cve_item_affect` (
  `cve` INT UNSIGNED NOT NULL,
  `digest` BINARY(20) NOT NULL,
  `vendor` TINYTEXT NOT NULL,
  `product` TINYTEXT NOT NULL,
  `version` TINYTEXT NOT NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`cve`,`digest`),
  INDEX `fk_cve_affect_has_cve` (`cve` ASC),
  CONSTRAINT `fk_cve_affect_has_cve`
    FOREIGN KEY (`cve`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `fk_cve_affect_has_generator` (`generator` ASC),
  CONSTRAINT `fk_cve_affect_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cve_item_configuration`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cve_item_configuration` (
  `cve` INT UNSIGNED NOT NULL,
  `id` INT UNSIGNED NOT NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`cve`,`id`),
  CONSTRAINT `fk_cve_configuration_has_cve`
    FOREIGN KEY (`cve`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `fk_cve_configuration_has_generator` (`generator` ASC),
  CONSTRAINT `fk_cve_configuration_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cve_item_configuration_disjunct`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cve_item_configuration_disjunct` (
  `cve` INT UNSIGNED NOT NULL,
  `configuration` INT UNSIGNED NOT NULL,
  `id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`cve`,`configuration`,`id`),
  CONSTRAINT `fk_cve_configuration_disjunct_has_cve_configuration`
    FOREIGN KEY (`cve`,`configuration`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_item_configuration` (`cve`,`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cve_item_configuration_conjunct`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cve_item_configuration_conjunct` (
  `cve` INT UNSIGNED NOT NULL,
  `configuration` INT UNSIGNED NOT NULL,
  `disjunct` INT UNSIGNED NOT NULL,
  `wfn` INT UNSIGNED NOT NULL,
  `vulnerable` BIT(1) NOT NULL,
  `opt_version_start` TINYTEXT,
  `opt_version_start_boundary` ENUM('I','E') NOT NULL DEFAULT 'I' ,
  `opt_version_end` TINYTEXT,
  `opt_version_end_boundary` ENUM('I','E') NOT NULL DEFAULT 'I',
  PRIMARY KEY (`cve`,`configuration`,`disjunct`,`wfn`),
  CONSTRAINT `fk_cve_configuration_conjunct_has_cve_configuration_disjunct`
    FOREIGN KEY (`cve`,`configuration`,`disjunct`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_item_configuration_disjunct` (`cve`,`configuration`,`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cve_configuration_conjunct_has_wfn`
    FOREIGN KEY (`wfn`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_wfn_item` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cve_item_impact_cvssv2`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cve_item_impact_cvssv2` (
  `cve` INT UNSIGNED NOT NULL,
  `cvss_av` ENUM('L','A','N') NOT NULL,
  `cvss_ac` ENUM('H','M','L') NOT NULL,
  `cvss_au` ENUM('M','S','N') NOT NULL,
  `cvss_c` ENUM('N','P','C') NOT NULL,
  `cvss_i` ENUM('N','P','C') NOT NULL,
  `cvss_a` ENUM('N','P','C') NOT NULL,
  `cvss_e` ENUM('U','POC','F','H','ND') NOT NULL,
  `cvss_rl` ENUM('OF','TF','W','U','ND') NOT NULL,
  `cvss_rc` ENUM('UC','UR','C','ND') NOT NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`cve`),
  CONSTRAINT `fk_cve_impact_cvssv2_has_cve`
    FOREIGN KEY (`cve`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `fk_cve_impact_cvssv2_has_generator` (`generator` ASC),
  CONSTRAINT `fk_cve_impact_cvssv2_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_cve_item_impact_cvssv3`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_cve_item_impact_cvssv3` (
  `cve` INT UNSIGNED NOT NULL,
  `cvss_av` ENUM('N','A','L','P') NOT NULL,
  `cvss_ac` ENUM('L','H') NOT NULL,
  `cvss_pr` ENUM('N','L','H') NOT NULL,
  `cvss_ui` ENUM('N','R') NOT NULL,
  `cvss_s` ENUM('U','C') NOT NULL,
  `cvss_c` ENUM('H','L','N') NOT NULL,
  `cvss_i` ENUM('H','L','N') NOT NULL,
  `cvss_a` ENUM('H','L','N') NOT NULL,
  `cvss_e` ENUM('X','H','F','P','U') NOT NULL,
  `cvss_rl` ENUM('X','U','W','T','O') NOT NULL,
  `cvss_rc` ENUM('X','C','R','U') NOT NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`cve`),
  CONSTRAINT `fk_cve_impact_cvssv3_has_cve`
    FOREIGN KEY (`cve`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `fk_cve_impact_cvssv3_has_generator` (`generator` ASC),
  CONSTRAINT `fk_cve_impact_cvssv3_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------------------------------------------------------
-- Asset `t_asset_` & `t_host_` related tables
-- ----------------------------------------------------------------------------
-- Table `{{DATA_DB_NAME}}`.`t_asset_generator`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_asset_generator` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `process_start_date` DATETIME NOT NULL,
  `process_end_date` DATETIME DEFAULT NULL,
  `server_name` VARCHAR(64) NULL,
  `server_version` VARCHAR(16) NULL,
  `agent_name` VARCHAR(64) NULL,
  `agent_version` VARCHAR(16) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_host_item`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_host_item` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `generator` INT UNSIGNED NOT NULL,
  `inet4_addr` INT UNSIGNED DEFAULT NULL,
  `inet4_mask` INT UNSIGNED DEFAULT NULL,
  `hostname` TINYTEXT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uk_inet4` (`inet4_addr`,`inet4_mask`),
  INDEX `fk_host_has_generator` (`generator` ASC),
  CONSTRAINT `fk_host_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_asset_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_asset_item`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_asset_item` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host` INT UNSIGNED NOT NULL,
  `wfn` INT UNSIGNED NULL,
  `generator` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uk_asset` (`host`,`wfn`),
  INDEX `fk_asset_has_host` (`host` ASC),
  INDEX `fk_asset_has_generator` (`generator` ASC),
  INDEX `fk_asset_has_wfn` (`wfn` ASC),
  CONSTRAINT `fk_asset_has_host`
    FOREIGN KEY (`host`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_host_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_asset_has_generator`
    FOREIGN KEY (`generator`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_asset_generator` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_asset_has_wfn`
    FOREIGN KEY (`wfn`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_wfn_item` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_asset_item_environment_cvssv2`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_asset_item_environment_cvssv2` (
  `asset` INT UNSIGNED NOT NULL,
  `cvss_cdp` ENUM('N','L','LM','MH','H','ND') NOT NULL,
  `cvss_td` ENUM('N','L','M','H','ND') NOT NULL,
  `cvss_cr` ENUM('L','M','H','ND') NOT NULL,
  `cvss_ir` ENUM('L','M','H','ND') NOT NULL,
  `cvss_ar` ENUM('L','M','H','ND') NOT NULL,
  PRIMARY KEY (`asset`),
  CONSTRAINT `fk_asset_environment_cvssv2_has_asset`
    FOREIGN KEY (`asset`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_asset_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table `{{DATA_DB_NAME}}`.`t_asset_item_environment_cvssv3`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_asset_item_environment_cvssv3` (
  `asset` INT UNSIGNED NOT NULL,
  `cvss_cr` ENUM('X','H','M','L') NOT NULL,
  `cvss_ir` ENUM('X','H','M','L') NOT NULL,
  `cvss_ar` ENUM('X','H','M','L') NOT NULL,
  `cvss_mav` ENUM('N','A','L','P') NOT NULL,
  `cvss_mac` ENUM('L','H') NOT NULL,
  `cvss_mpr` ENUM('N','L','H') NOT NULL,
  `cvss_mui` ENUM('N','R') NOT NULL,
  `cvss_ms` ENUM('U','C') NOT NULL,
  `cvss_mc` ENUM('H','L','N') NOT NULL,
  `cvss_mi` ENUM('H','L','N') NOT NULL,
  `cvss_ma` ENUM('H','L','N') NOT NULL,
  PRIMARY KEY (`asset`),
  CONSTRAINT `fk_asset_environment_cvssv3_has_asset`
    FOREIGN KEY (`asset`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_asset_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------------------------------------------------------
-- Assessment related tables
-- ----------------------------------------------------------------------------
-- Table `{{DATA_DB_NAME}}`.`t_assessment`
CREATE TABLE IF NOT EXISTS `{{DATA_DB_NAME}}`.`t_assessment` (
  `asset` INT UNSIGNED NOT NULL,
  `cve` INT UNSIGNED NOT NULL,
  `timestamp` DATETIME NOT NULL,
  `cvssv2_score` FLOAT(4,2) UNSIGNED NOT NULL,
  `cvssv3_score` FLOAT(4,2) UNSIGNED NOT NULL,
  PRIMARY KEY (`asset`, `cve`),
  INDEX `fk_assessment_has_cve` (`cve` ASC),
  INDEX `fk_assessment_has_asset` (`asset` ASC),
  CONSTRAINT `fk_assessment_has_cve`
    FOREIGN KEY (`cve`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_cve_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_assessment_has_asset`
    FOREIGN KEY (`asset`)
    REFERENCES `{{DATA_DB_NAME}}`.`t_asset_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- Routines
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- ----------------------------------------------------------------------------
-- Helpers
-- ----------------------------------------------------------------------------
-- Compute digest in order to replace SQL unique constraint
DELIMITER //
CREATE FUNCTION `{{DATA_DB_NAME}}`.`f_compute_digest`(
	data LONGTEXT
)
RETURNS BINARY(20)
LANGUAGE SQL
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER
COMMENT 'Return a BINARY(20) SHA1 digest of data.'
BEGIN
  RETURN UNHEX(SHA1(data));
END//
DELIMITER ;

-- Transform a WFN parameter string in order to be used with SQL LIKE
DELIMITER //
CREATE FUNCTION `{{DATA_DB_NAME}}`.`f_wfn_param_to_sql_like`(
	`param` TINYTEXT
)
RETURNS TINYTEXT CHARSET utf8 COLLATE utf8_unicode_ci
LANGUAGE SQL
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER
COMMENT 'Transform a WFN parameter in order prepare a SQL LIKE comparison. Return a TINYTEXT string.'
BEGIN
  RETURN REPLACE(REPLACE(REPLACE(REPLACE(param, "%", "\%"), "*", "%"), "_", "\_"), "?", "_");
END//
DELIMITER ;

-- Check if a CVE configuration conjunction match a host configuration
DELIMITER //
CREATE FUNCTION `{{DATA_DB_NAME}}`.`f_is_cve_match_host`(
	`cve_id` INT,
  `cve_configuration` INT,
  `cve_disjunct` INT,
  `host` INT
)
RETURNS BINARY
LANGUAGE SQL
NOT DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER
COMMENT 'Return TRUE if the given CVE configuration match the given host configuration.'
BEGIN
  -- Working data
  DECLARE cve_cfg_count INT;
  DECLARE host_cfg_match INT;
  -- Retrieve the number of CVE configuration conjunct entries that have to match host configuration
  SELECT
    COUNT(*) INTO cve_cfg_count
  FROM `t_cve_item_configuration_conjunct`
  WHERE `t_cve_item_configuration_conjunct`.`cve` = cve_id
  AND `t_cve_item_configuration_conjunct`.`configuration` = cve_configuration
  AND `t_cve_item_configuration_conjunct`.`disjunct` = cve_disjunct;
  -- Retrieve the number of distinct CVE configuration conjunct entries that match host configuration
  SELECT
    COUNT(*) INTO host_cfg_match FROM (
      SELECT
        `t_cve_item_configuration_conjunct`.`wfn`
      FROM `t_cve_item_configuration_conjunct`
      -- JOIN conjunct WFN details
      INNER JOIN `t_wfn_item` AS `conjunct_configuration`
        ON `conjunct_configuration`.`id` = `t_cve_item_configuration_conjunct`.`wfn`
      -- JOIN asset list from host
      INNER JOIN `t_asset_item` AS `asset_list`
        ON `asset_list`.`host` = host
      -- JOIN asset WFN details -- JOIN it only if it match at least one conjunct WFN
      INNER JOIN `t_wfn_item` AS `host_configuration`
        ON `host_configuration`.`id` = `asset_list`.`wfn`
        AND `host_configuration`.`part` = `conjunct_configuration`.`part`
        AND `host_configuration`.`vendor` like f_wfn_param_to_sql_like(`conjunct_configuration`.`vendor`)
        AND `host_configuration`.`product` like f_wfn_param_to_sql_like(`conjunct_configuration`.`product`)
        AND `host_configuration`.`version` like f_wfn_param_to_sql_like(`conjunct_configuration`.`version`)
        AND `host_configuration`.`update` like f_wfn_param_to_sql_like(`conjunct_configuration`.`update`)
        AND `host_configuration`.`edition` like f_wfn_param_to_sql_like(`conjunct_configuration`.`edition`)
        AND `host_configuration`.`sw_edition` like f_wfn_param_to_sql_like(`conjunct_configuration`.`sw_edition`)
        AND `host_configuration`.`target_sw` like f_wfn_param_to_sql_like(`conjunct_configuration`.`target_sw`)
        AND `host_configuration`.`target_hw` like f_wfn_param_to_sql_like(`conjunct_configuration`.`target_hw`)
      WHERE `t_cve_item_configuration_conjunct`.`cve` = cve_id
      AND `t_cve_item_configuration_conjunct`.`configuration` = cve_configuration
      AND `t_cve_item_configuration_conjunct`.`disjunct` = cve_disjunct
      GROUP BY `conjunct_configuration`.`id`
    ) AS subquery;
  -- Return results
  IF cve_cfg_count > 0 THEN
    RETURN cve_cfg_count = host_cfg_match;
  ELSE
    RETURN 0;
  END IF;
END//
DELIMITER ;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- Views
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- ----------------------------------------------------------------------------
-- CVE related views
-- ----------------------------------------------------------------------------
-- View `{{DATA_DB_NAME}}`.`v_cve`
CREATE OR REPLACE
  ALGORITHM=UNDEFINED
  DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
  VIEW `{{DATA_DB_NAME}}`.`v_cve` AS
  SELECT
    `t_cve_item`.`id` AS `id`,
    `t_cve_item`.`year` AS `year`,
    `t_cve_item`.`digits` AS `digits`,
    `t_cve_item`.`published_date` AS `published_date`,
    `t_cve_item`.`modified_date` AS `modified_date`,
    `t_cve_generator`.`cve_data_timestamp` AS `source_date`,
    `t_cve_generator`.`cve_data_type` AS `source_type`,
    `t_cve_generator`.`cve_data_format` AS `source_format`,
    `t_cve_generator`.`cve_data_version` AS `source_version`,
    `t_cve_item_impact_cvssv2`.`cvss_av` AS `cvssv2_av`,
    `t_cve_item_impact_cvssv2`.`cvss_ac` AS `cvssv2_ac`,
    `t_cve_item_impact_cvssv2`.`cvss_au` AS `cvssv2_au`,
    `t_cve_item_impact_cvssv2`.`cvss_c` AS `cvssv2_c`,
    `t_cve_item_impact_cvssv2`.`cvss_i` AS `cvssv2_i`,
    `t_cve_item_impact_cvssv2`.`cvss_a` AS `cvssv2_a`,
    `t_cve_item_impact_cvssv2`.`cvss_e` AS `cvssv2_e`,
    `t_cve_item_impact_cvssv2`.`cvss_rl` AS `cvssv2_rl`,
    `t_cve_item_impact_cvssv2`.`cvss_rc` AS `cvssv2_rc`,
    `t_cve_item_impact_cvssv3`.`cvss_av` AS `cvssv3_av`,
    `t_cve_item_impact_cvssv3`.`cvss_ac` AS `cvssv3_ac`,
    `t_cve_item_impact_cvssv3`.`cvss_pr` AS `cvssv3_pr`,
    `t_cve_item_impact_cvssv3`.`cvss_ui` AS `cvssv3_ui`,
    `t_cve_item_impact_cvssv3`.`cvss_s` AS `cvssv3_s`,
    `t_cve_item_impact_cvssv3`.`cvss_c` AS `cvssv3_c`,
    `t_cve_item_impact_cvssv3`.`cvss_i` AS `cvssv3_i`,
    `t_cve_item_impact_cvssv3`.`cvss_a` AS `cvssv3_a`,
    `t_cve_item_impact_cvssv3`.`cvss_e` AS `cvssv3_e`,
    `t_cve_item_impact_cvssv3`.`cvss_rl` AS `cvssv3_rl`,
    `t_cve_item_impact_cvssv3`.`cvss_rc` AS `cvssv3_rc`
  FROM `t_cve_item`
  INNER JOIN `t_cve_generator` ON `t_cve_item`.`generator` = `t_cve_generator`.`id`
  LEFT JOIN `t_cve_item_impact_cvssv2` ON `t_cve_item`.`id` = `t_cve_item_impact_cvssv2`.`cve`
  LEFT JOIN `t_cve_item_impact_cvssv3` ON `t_cve_item`.`id` = `t_cve_item_impact_cvssv3`.`cve`;

-- View `{{DATA_DB_NAME}}`.`v_cve_description`
CREATE OR REPLACE
  ALGORITHM=UNDEFINED
  DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
  VIEW `{{DATA_DB_NAME}}`.`v_cve_description` AS
  SELECT
    `t_cve_item_description`.`cve` AS `cve`,
    `t_cve_item_description`.`lang` AS `lang`,
    `t_cve_item_description`.`value` AS `value`
  FROM `t_cve_item_description`;

-- View `{{DATA_DB_NAME}}`.`v_cve_reference`
CREATE OR REPLACE
  ALGORITHM=UNDEFINED
  DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
  VIEW `{{DATA_DB_NAME}}`.`v_cve_reference` AS
  SELECT
    `t_cve_item_reference`.`cve` AS `cve`,
    `t_cve_item_reference`.`url` AS `href`
  FROM `t_cve_item_reference`;

  -- View `{{DATA_DB_NAME}}`.`v_cve_affect`
  CREATE OR REPLACE
    ALGORITHM=UNDEFINED
    DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
    VIEW `{{DATA_DB_NAME}}`.`v_cve_affect` AS
    SELECT
      `t_cve_item_affect`.`cve` AS `cve`,
      `t_cve_item_affect`.`vendor` AS `vendor`,
      `t_cve_item_affect`.`product` AS `product`,
      `t_cve_item_affect`.`version` AS `version`
    FROM `t_cve_item_affect`;

-- ----------------------------------------------------------------------------
-- CPE related views
-- ----------------------------------------------------------------------------
-- View `{{DATA_DB_NAME}}`.`v_cpe`
CREATE OR REPLACE
  ALGORITHM=UNDEFINED
  DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
  VIEW `{{DATA_DB_NAME}}`.`v_cpe` AS
  SELECT
    `t_cpe_item`.`wfn` AS `id`,
    `t_cpe_item`.`deprecated` AS `deprecated`,
    `t_cpe_item`.`deprecation_date` AS `deprecation_date`,
    `t_cpe_generator`.`cpe_data_timestamp` AS `source_date`,
    `t_cpe_generator`.`cpe_schema_version` AS `source_format`,
    `t_cpe_generator`.`cpe_product_name` AS `source_name`,
    `t_cpe_generator`.`cpe_product_version` AS `source_version`,
    `t_wfn_item`.`part` AS `wfn_part`,
    `t_wfn_item`.`vendor` AS `wfn_vendor`,
    `t_wfn_item`.`product` AS `wfn_product`,
    `t_wfn_item`.`version` AS `wfn_version`,
    `t_wfn_item`.`update` AS `wfn_update`,
    `t_wfn_item`.`edition` AS `wfn_edition`,
    `t_wfn_item`.`sw_edition` AS `wfn_sw_edition`,
    `t_wfn_item`.`target_sw` AS `wfn_target_sw`,
    `t_wfn_item`.`target_hw` AS `wfn_target_hw`
  FROM `t_cpe_item`
  INNER JOIN `t_cpe_generator` ON `t_cpe_item`.`generator` = `t_cpe_generator`.`id`
  INNER JOIN `t_wfn_item` ON `t_cpe_item`.`wfn` = `t_wfn_item`.`id`;

-- View `{{DATA_DB_NAME}}`.`v_cpe_title`
CREATE OR REPLACE
  ALGORITHM=UNDEFINED
  DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
  VIEW `{{DATA_DB_NAME}}`.`v_cpe_title` AS
  SELECT
    `t_cpe_item_title`.`cpe` AS `cpe`,
    `t_cpe_item_title`.`lang` AS `lang`,
    `t_cpe_item_title`.`value` AS `value`
  FROM `t_cpe_item_title`;

-- View `{{DATA_DB_NAME}}`.`v_cpe_reference`
CREATE OR REPLACE
  ALGORITHM=UNDEFINED
  DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
  VIEW `{{DATA_DB_NAME}}`.`v_cpe_reference` AS
  SELECT
    `t_cpe_item_reference`.`cpe` AS `cpe`,
    `t_cpe_item_reference`.`href` AS `href`,
    `t_cpe_item_reference`.`value` AS `value`
  FROM `t_cpe_item_reference`;

  -- View `{{DATA_DB_NAME}}`.`v_cpe_deprecated_by`
  CREATE OR REPLACE
    ALGORITHM=UNDEFINED
    DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
    VIEW `{{DATA_DB_NAME}}`.`v_cpe_deprecated_by` AS
    SELECT
      `t_cpe_item_deprecated_by`.`cpe` AS `cpe`,
      `t_cpe_item`.`wfn` AS `wfn`,
      `t_cpe_item_deprecated_by`.`date` AS `date`,
      `t_cpe_item_deprecated_by`.`type` AS `type`
    FROM `t_cpe_item_deprecated_by`
    INNER JOIN `t_cpe_item` ON `t_cpe_item_deprecated_by`.`wfn` = `t_cpe_item`.`wfn`;

-- ----------------------------------------------------------------------------
-- Asset related views
-- ----------------------------------------------------------------------------
-- View `{{DATA_DB_NAME}}`.`v_host`
CREATE OR REPLACE
  ALGORITHM=UNDEFINED
  DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
  VIEW `{{DATA_DB_NAME}}`.`v_host` AS
  SELECT
    `t_host_item`.`id` AS `id`,
    INET_NTOA(`t_host_item`.`inet4_addr`) AS `inet4_addr`,
    INET_NTOA(`t_host_item`.`inet4_mask`) AS `inet4_mask`,
    `t_host_item`.`hostname` AS `hostname`,
    `t_asset_generator`.`process_start_date` AS `source_date`,
    `t_asset_generator`.`agent_name` AS `source_name`,
    `t_asset_generator`.`agent_version` AS `source_version`
  FROM `t_host_item`
  INNER JOIN `t_asset_generator` ON `t_host_item`.`generator` = `t_asset_generator`.`id`;

-- View `{{DATA_DB_NAME}}`.`v_asset`
CREATE OR REPLACE
  ALGORITHM=UNDEFINED
  DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
  VIEW `{{DATA_DB_NAME}}`.`v_asset` AS
  SELECT
    `t_asset_item`.`id` AS `id`,
    `t_asset_item`.`host` AS `host`,
    `t_wfn_item`.`part` AS `wfn_part`,
    `t_wfn_item`.`vendor` AS `wfn_vendor`,
    `t_wfn_item`.`product` AS `wfn_product`,
    `t_wfn_item`.`version` AS `wfn_version`,
    `t_wfn_item`.`update` AS `wfn_update`,
    `t_wfn_item`.`edition` AS `wfn_edition`,
    `t_wfn_item`.`sw_edition` AS `wfn_sw_edition`,
    `t_wfn_item`.`target_sw` AS `wfn_target_sw`,
    `t_wfn_item`.`target_hw` AS `wfn_target_hw`,
    `t_asset_generator`.`process_start_date` AS `source_date`,
    `t_asset_generator`.`agent_name` AS `source_name`,
    `t_asset_generator`.`agent_version` AS `source_version`
  FROM `t_asset_item`
  INNER JOIN `t_asset_generator` ON `t_asset_item`.`generator` = `t_asset_generator`.`id`
  LEFT JOIN `t_wfn_item` ON `t_asset_item`.`wfn` = `t_wfn_item`.`id`;

-- ----------------------------------------------------------------------------
-- Assessment related views
-- ----------------------------------------------------------------------------
-- View `{{DATA_DB_NAME}}`.`v_assessment`
CREATE OR REPLACE
  ALGORITHM=UNDEFINED
  DEFINER=`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`
  VIEW `{{DATA_DB_NAME}}`.`v_assessment` AS
  SELECT
  `t_assessment`.`asset` AS `asset`,
  `t_asset_item`.`host` AS `host`,
  `t_assessment`.`cve` AS `cve`,
  `t_cve_item`.`year` AS `year`,
  `t_cve_item`.`digits` AS `digits`,
  `t_assessment`.`timestamp` AS `date`,
  `t_assessment`.`cvssv2_score` AS `cvssv2_score`,
  `t_assessment`.`cvssv3_score` AS `cvssv3_score`
  FROM `t_assessment`
  INNER JOIN `t_cve_item` ON `t_cve_item`.`id` = `t_assessment`.`cve`
  INNER JOIN `t_asset_item` ON `t_asset_item`.`id` = `t_assessment`.`asset`;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- Stored procedures
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- ----------------------------------------------------------------------------
-- CPE related procedures
-- ----------------------------------------------------------------------------
-- Procedure `{{DATA_DB_NAME}}`.`p_cpe_start_update_process`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_start_update_process`(
  IN cpe_data_timestamp DATETIME,
  IN cpe_schema_version TINYTEXT,
  IN cpe_product_name TINYTEXT,
  IN cpe_product_version TINYTEXT,
  OUT generator_id INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Create a new CPE generator entry and save its id into "generator_id".'
BEGIN
  INSERT INTO `t_cpe_generator` (
    `t_cpe_generator`.`process_start_date`,
    `t_cpe_generator`.`cpe_data_timestamp`,
    `t_cpe_generator`.`cpe_schema_version`,
    `t_cpe_generator`.`cpe_product_name`,
    `t_cpe_generator`.`cpe_product_version`
  )
  VALUES (
    NOW(),
    cpe_data_timestamp,
    cpe_schema_version,
    cpe_product_name,
    cpe_product_version
  );
  -- Retrieve last inserted generator id
  SELECT LAST_INSERT_ID() INTO generator_id;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cpe_save_cpe`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_save_cpe`(
  IN wfn_part ENUM('a','o','h'),
  IN wfn_vendor TINYTEXT,
  IN wfn_product TINYTEXT,
  IN wfn_version TINYTEXT,
  IN wfn_update TINYTEXT,
  IN wfn_edition TINYTEXT,
  IN wfn_sw_edition TINYTEXT,
  IN wfn_target_sw TINYTEXT,
  IN wfn_target_hw TINYTEXT,
  IN cpe_deprecated TINYINT(1),
  IN cpe_deprecation_date DATETIME,
  IN generator INT UNSIGNED,
  OUT cpe_id INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save a CPE item and save corresponding WFN item if needed. CPE item id is saved into "cpe_id".'
BEGIN
  -- Save WFN item if needed
  INSERT INTO `t_wfn_item` (
    `t_wfn_item`.`digest`,
    `t_wfn_item`.`part`,
    `t_wfn_item`.`vendor`,
    `t_wfn_item`.`product`,
    `t_wfn_item`.`version`,
    `t_wfn_item`.`update`,
    `t_wfn_item`.`edition`,
    `t_wfn_item`.`sw_edition`,
    `t_wfn_item`.`target_sw`,
    `t_wfn_item`.`target_hw`
  )
  VALUES (
    f_compute_digest(CONCAT(wfn_part,wfn_vendor,wfn_product,wfn_version,wfn_update,wfn_edition,wfn_sw_edition,wfn_target_sw,wfn_target_hw)),
    wfn_part,
    wfn_vendor,
    wfn_product,
    wfn_version,
    wfn_update,
    wfn_edition,
    wfn_sw_edition,
    wfn_target_sw,
    wfn_target_hw
  )
  ON DUPLICATE KEY UPDATE
    `t_wfn_item`.`id` = LAST_INSERT_ID(`t_wfn_item`.`id`);
  -- Retrieve last inserted wfn id
  SELECT LAST_INSERT_ID() INTO cpe_id;
  -- Then save or update CPE item
  INSERT INTO `t_cpe_item` (
    `t_cpe_item`.`wfn`,
    `t_cpe_item`.`deprecated`,
    `t_cpe_item`.`deprecation_date`,
    `t_cpe_item`.`generator`
  )
  VALUES (
    cpe_id,
    cpe_deprecated,
    cpe_deprecation_date,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_cpe_item`.`deprecated` = cpe_deprecated,
    `t_cpe_item`.`deprecation_date` = cpe_deprecation_date,
    `t_cpe_item`.`generator` = generator;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cpe_save_cpe_title`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_save_cpe_title`(
  IN cpe INT UNSIGNED,
  IN lang CHAR(5),
  IN value MEDIUMTEXT,
  IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save a CPE item title and replace existing title if any.'
BEGIN
  INSERT INTO `t_cpe_item_title` (
    `t_cpe_item_title`.`cpe`,
    `t_cpe_item_title`.`digest`,
    `t_cpe_item_title`.`lang`,
    `t_cpe_item_title`.`value`,
    `t_cpe_item_title`.`generator`
  )
  VALUES (
    cpe,
    f_compute_digest(CONCAT(lang, value)),
    lang,
    value,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_cpe_item_title`.`generator` = generator;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cpe_save_cpe_reference`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_save_cpe_reference`(
  IN cpe INT UNSIGNED,
  IN href VARCHAR(2083),
  IN value MEDIUMTEXT,
  IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save a CPE item reference and replace existing if any.'
BEGIN
  INSERT INTO `t_cpe_item_reference` (
    `t_cpe_item_reference`.`cpe`,
    `t_cpe_item_reference`.`digest`,
    `t_cpe_item_reference`.`href`,
    `t_cpe_item_reference`.`value`,
    `t_cpe_item_reference`.`generator`
  )
  VALUES (
    cpe,
    f_compute_digest(CONCAT(href, value)),
    href,
    value,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_cpe_item_reference`.`generator` = generator;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cpe_save_cpe_deprecated_by`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_save_cpe_deprecated_by`(
  IN cpe INT UNSIGNED,
  IN wfn_part ENUM('a','o','h'),
  IN wfn_vendor TINYTEXT,
  IN wfn_product TINYTEXT,
  IN wfn_version TINYTEXT,
  IN wfn_update TINYTEXT,
  IN wfn_edition TINYTEXT,
  IN wfn_sw_edition TINYTEXT,
  IN wfn_target_sw TINYTEXT,
  IN wfn_target_hw TINYTEXT,
  IN deprecation_date DATETIME,
  IN deprecation_type TINYTEXT,
  IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save a CPE item deprecated_by and replace existing if any.'
BEGIN
  DECLARE wfn_id INT UNSIGNED;
  -- Save WFN item if needed
  INSERT INTO `t_wfn_item` (
    `t_wfn_item`.`digest`,
    `t_wfn_item`.`part`,
    `t_wfn_item`.`vendor`,
    `t_wfn_item`.`product`,
    `t_wfn_item`.`version`,
    `t_wfn_item`.`update`,
    `t_wfn_item`.`edition`,
    `t_wfn_item`.`sw_edition`,
    `t_wfn_item`.`target_sw`,
    `t_wfn_item`.`target_hw`
  )
  VALUES (
    f_compute_digest(CONCAT(wfn_part,wfn_vendor,wfn_product,wfn_version,wfn_update,wfn_edition,wfn_sw_edition,wfn_target_sw,wfn_target_hw)),
    wfn_part,
    wfn_vendor,
    wfn_product,
    wfn_version,
    wfn_update,
    wfn_edition,
    wfn_sw_edition,
    wfn_target_sw,
    wfn_target_hw
  )
  ON DUPLICATE KEY UPDATE
    `t_wfn_item`.`id` = LAST_INSERT_ID(`t_wfn_item`.`id`);
  -- Retrieve last inserted wfn id
  SELECT LAST_INSERT_ID() INTO wfn_id;
  -- Save CPE deprecated_by
  INSERT INTO `t_cpe_item_deprecated_by` (
    `t_cpe_item_deprecated_by`.`cpe`,
    `t_cpe_item_deprecated_by`.`digest`,
    `t_cpe_item_deprecated_by`.`wfn`,
    `t_cpe_item_deprecated_by`.`date`,
    `t_cpe_item_deprecated_by`.`type`,
    `t_cpe_item_deprecated_by`.`generator`
  )
  VALUES (
    cpe,
    f_compute_digest(CONCAT(wfn_id,deprecation_date,deprecation_type)),
    wfn_id,
    deprecation_date,
    deprecation_type,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_cpe_item_deprecated_by`.`generator` = generator;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cpe_complete_update_process`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_complete_update_process`(
  IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Update a CPE generator entry and save the update process end date.'
BEGIN
  UPDATE `t_cpe_generator`
    SET `t_cpe_generator`.`process_end_date` = NOW()
    WHERE `t_cpe_generator`.`id` = generator;
END//
DELIMITER ;

-- ----------------------------------------------------------------------------
-- CVE related procedures
-- ----------------------------------------------------------------------------
-- Procedure `{{DATA_DB_NAME}}`.`p_cve_start_update_process`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_start_update_process`(
  IN cve_data_timestamp DATETIME,
  IN cve_data_type TINYTEXT,
  IN cve_data_format TINYTEXT,
  IN cve_data_version TINYTEXT,
  OUT generator_id INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Create a new CVE generator entry and save its id into "generator_id".'
BEGIN
  INSERT INTO `t_cve_generator` (
    `t_cve_generator`.`process_start_date`,
    `t_cve_generator`.`cve_data_timestamp`,
    `t_cve_generator`.`cve_data_type`,
    `t_cve_generator`.`cve_data_format`,
    `t_cve_generator`.`cve_data_version`
  )
  VALUES (
    NOW(),
    cve_data_timestamp,
    cve_data_type,
    cve_data_format,
    cve_data_version
  );
  -- Retrieve last inserted generator id
  SELECT LAST_INSERT_ID() INTO generator_id;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_save_cve`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve`(
  IN year CHAR(4),
  IN digits VARCHAR(32),
  IN published_date DATETIME,
  IN modified_date DATETIME,
  IN generator INT UNSIGNED,
  OUT cve_id INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save a CVE item. CVE item id is saved into "cve_id".'
BEGIN
  INSERT INTO `t_cve_item` (
    `t_cve_item`.`year`,
    `t_cve_item`.`digits`,
    `t_cve_item`.`published_date`,
    `t_cve_item`.`modified_date`,
    `t_cve_item`.`generator`
  )
  VALUES (
    year,
    digits,
    published_date,
    modified_date,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_cve_item`.`id` = LAST_INSERT_ID(`t_cve_item`.`id`),
    `t_cve_item`.`year` = year,
    `t_cve_item`.`digits` = digits,
    `t_cve_item`.`published_date` = published_date,
    `t_cve_item`.`modified_date` = modified_date,
    `t_cve_item`.`generator` = generator;
  -- Retrieve last inserted CVE id
  SELECT LAST_INSERT_ID() INTO cve_id;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_save_cve_impact_cvssv2`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve_impact_cvssv2`(
  IN cve INT UNSIGNED,
  IN cvss_av ENUM('L','A','N'),
	IN cvss_ac ENUM('H','M','L'),
	IN cvss_au ENUM('M','S','N'),
	IN cvss_c ENUM('N','P','C'),
	IN cvss_i ENUM('N','P','C'),
	IN cvss_a ENUM('N','P','C'),
	IN cvss_e ENUM('U','POC','F','H','ND'),
	IN cvss_rl ENUM('OF','TF','W','U','ND'),
	IN cvss_rc ENUM('UC','UR','C','ND'),
	IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save or update a CVE item CVSSv2 impact.'
BEGIN
  INSERT INTO `t_cve_item_impact_cvssv2` (
    `t_cve_item_impact_cvssv2`.`cve`,
    `t_cve_item_impact_cvssv2`.`cvss_av`,
    `t_cve_item_impact_cvssv2`.`cvss_ac`,
    `t_cve_item_impact_cvssv2`.`cvss_au`,
    `t_cve_item_impact_cvssv2`.`cvss_c`,
    `t_cve_item_impact_cvssv2`.`cvss_i`,
    `t_cve_item_impact_cvssv2`.`cvss_a`,
    `t_cve_item_impact_cvssv2`.`cvss_e`,
    `t_cve_item_impact_cvssv2`.`cvss_rl`,
    `t_cve_item_impact_cvssv2`.`cvss_rc`,
    `t_cve_item_impact_cvssv2`.`generator`
  )
  VALUES (
    cve,
    cvss_av,
    cvss_ac,
    cvss_au,
    cvss_c,
    cvss_i,
    cvss_a,
    cvss_e,
    cvss_rl,
    cvss_rc,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_cve_item_impact_cvssv2`.`cvss_av` = cvss_av,
    `t_cve_item_impact_cvssv2`.`cvss_ac` = cvss_ac,
    `t_cve_item_impact_cvssv2`.`cvss_au` = cvss_au,
    `t_cve_item_impact_cvssv2`.`cvss_c` = cvss_c,
    `t_cve_item_impact_cvssv2`.`cvss_i` = cvss_i,
    `t_cve_item_impact_cvssv2`.`cvss_a` = cvss_a,
    `t_cve_item_impact_cvssv2`.`cvss_e` = cvss_e,
    `t_cve_item_impact_cvssv2`.`cvss_rl` = cvss_rl,
    `t_cve_item_impact_cvssv2`.`cvss_rc` = cvss_rc,
    `t_cve_item_impact_cvssv2`.`generator` = generator;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_save_cve_impact_cvssv3`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve_impact_cvssv3`(
  IN cve INT UNSIGNED,
  IN cvss_av ENUM('N','A','L','P'),
  IN cvss_ac ENUM('L','H'),
  IN cvss_pr ENUM('N','L','H'),
  IN cvss_ui ENUM('N','R'),
  IN cvss_s ENUM('U','C'),
  IN cvss_c ENUM('H','L','N'),
  IN cvss_i ENUM('H','L','N'),
  IN cvss_a ENUM('H','L','N'),
  IN cvss_e ENUM('X','H','F','P','U'),
  IN cvss_rl ENUM('X','U','W','T','O'),
  IN cvss_rc ENUM('X','C','R','U'),
  IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save or update a CVE item CVSSv3 impact.'
BEGIN
  INSERT INTO `t_cve_item_impact_cvssv3` (
    `t_cve_item_impact_cvssv3`.`cve`,
    `t_cve_item_impact_cvssv3`.`cvss_av`,
    `t_cve_item_impact_cvssv3`.`cvss_ac`,
    `t_cve_item_impact_cvssv3`.`cvss_pr`,
    `t_cve_item_impact_cvssv3`.`cvss_ui`,
    `t_cve_item_impact_cvssv3`.`cvss_s`,
    `t_cve_item_impact_cvssv3`.`cvss_c`,
    `t_cve_item_impact_cvssv3`.`cvss_i`,
    `t_cve_item_impact_cvssv3`.`cvss_a`,
    `t_cve_item_impact_cvssv3`.`cvss_e`,
    `t_cve_item_impact_cvssv3`.`cvss_rl`,
    `t_cve_item_impact_cvssv3`.`cvss_rc`,
    `t_cve_item_impact_cvssv3`.`generator`
  )
  VALUES (
    cve,
    cvss_av,
    cvss_ac,
    cvss_pr,
    cvss_ui,
    cvss_s,
    cvss_c,
    cvss_i,
    cvss_a,
    cvss_e,
    cvss_rl,
    cvss_rc,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_cve_item_impact_cvssv3`.`cvss_av` = cvss_av,
    `t_cve_item_impact_cvssv3`.`cvss_ac` = cvss_ac,
    `t_cve_item_impact_cvssv3`.`cvss_pr` = cvss_pr,
    `t_cve_item_impact_cvssv3`.`cvss_ui` = cvss_ui,
    `t_cve_item_impact_cvssv3`.`cvss_s` = cvss_s,
    `t_cve_item_impact_cvssv3`.`cvss_c` = cvss_c,
    `t_cve_item_impact_cvssv3`.`cvss_i` = cvss_i,
    `t_cve_item_impact_cvssv3`.`cvss_a` = cvss_a,
    `t_cve_item_impact_cvssv3`.`cvss_e` = cvss_e,
    `t_cve_item_impact_cvssv3`.`cvss_rl` = cvss_rl,
    `t_cve_item_impact_cvssv3`.`cvss_rc` = cvss_rc,
    `t_cve_item_impact_cvssv3`.`generator` = generator;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_save_cve_description`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve_description`(
  IN cve INT UNSIGNED,
  IN lang CHAR(5),
  IN value MEDIUMTEXT,
  IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save a CVE item description and replace existing if any.'
BEGIN
  INSERT INTO `t_cve_item_description` (
    `t_cve_item_description`.`cve`,
    `t_cve_item_description`.`digest`,
    `t_cve_item_description`.`lang`,
    `t_cve_item_description`.`value`,
    `t_cve_item_description`.`generator`
  )
  VALUES (
    cve,
    f_compute_digest(CONCAT(lang, value)),
    lang,
    value,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_cve_item_description`.`generator` = generator;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_save_cve_reference`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve_reference`(
  IN cve INT UNSIGNED,
  IN url VARCHAR(2083),
  IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save a CVE item reference and replace existing if any.'
BEGIN
  INSERT INTO `t_cve_item_reference` (
    `t_cve_item_reference`.`cve`,
    `t_cve_item_reference`.`digest`,
    `t_cve_item_reference`.`url`,
    `t_cve_item_reference`.`generator`
  )
  VALUES (
    cve,
    f_compute_digest(url),
    url,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_cve_item_reference`.`generator` = generator;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_save_cve_affect`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve_affect`(
  IN cve INT UNSIGNED,
  IN vendor TINYTEXT,
  IN product TINYTEXT,
  IN version TINYTEXT,
  IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save a CVE item affected product and replace existing if any.'
BEGIN
  INSERT INTO `t_cve_item_affect` (
    `t_cve_item_affect`.`cve`,
    `t_cve_item_affect`.`digest`,
    `t_cve_item_affect`.`vendor`,
    `t_cve_item_affect`.`product`,
    `t_cve_item_affect`.`version`,
    `t_cve_item_affect`.`generator`
  )
  VALUES (
    cve,
    f_compute_digest(CONCAT(vendor, product, version)),
    vendor,
    product,
    version,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_cve_item_affect`.`generator` = generator;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_configuration_flush`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_configuration_flush`(
  IN cve INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Erase all configuration entry of a given CVE. It delete disjunct and conjunct entries too.'
BEGIN
  DELETE FROM `t_cve_item_configuration`
    WHERE  `t_cve_item_configuration`.`cve` = cve;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_configuration_new`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_configuration_new`(
  IN cve INT UNSIGNED,
  IN generator INT UNSIGNED,
  OUT configuration_id INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Create a new configuration entry for given CVE'
BEGIN
  INSERT INTO `t_cve_item_configuration` (
    `t_cve_item_configuration`.`cve`,
    `t_cve_item_configuration`.`id`,
    `t_cve_item_configuration`.`generator`
  )
  SELECT
    cve, coalesce(MAX(`t_cve_item_configuration`.`id`), 0)+1, generator
  FROM `t_cve_item_configuration`
  WHERE `t_cve_item_configuration`.`cve` = cve;
  -- Retrieve last inserted configuration id
  SELECT MAX(`t_cve_item_configuration`.`id`)
  INTO configuration_id
  FROM `t_cve_item_configuration`
  WHERE `t_cve_item_configuration`.`cve` = cve;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_configuration_add_disjunction`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_configuration_add_disjunction`(
  IN cve INT UNSIGNED,
  IN configuration INT UNSIGNED,
  OUT disjunct_id INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Create a new configuration entry for given CVE'
BEGIN
  INSERT INTO `t_cve_item_configuration_disjunct` (
    `t_cve_item_configuration_disjunct`.`cve`,
    `t_cve_item_configuration_disjunct`.`configuration`,
    `t_cve_item_configuration_disjunct`.`id`
  )
  SELECT
    cve, configuration, coalesce(MAX(`t_cve_item_configuration_disjunct`.`id`), 0)+1
  FROM `t_cve_item_configuration_disjunct`
  WHERE `t_cve_item_configuration_disjunct`.`cve` = cve
  AND `t_cve_item_configuration_disjunct`.`configuration` = configuration;
  -- Retrieve last inserted disjunct id
  SELECT MAX(`t_cve_item_configuration_disjunct`.`id`)
  INTO disjunct_id
  FROM `t_cve_item_configuration_disjunct`
  WHERE `t_cve_item_configuration_disjunct`.`cve` = cve
  AND `t_cve_item_configuration_disjunct`.`configuration` = configuration;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_configuration_add_conjunction`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_configuration_add_conjunction`(
  IN cve INT UNSIGNED,
  IN configuration INT UNSIGNED,
  IN disjunct INT UNSIGNED,
  IN wfn_part ENUM('a','o','h'),
  IN wfn_vendor TINYTEXT,
  IN wfn_product TINYTEXT,
  IN wfn_version TINYTEXT,
  IN wfn_update TINYTEXT,
  IN wfn_edition TINYTEXT,
  IN wfn_sw_edition TINYTEXT,
  IN wfn_target_sw TINYTEXT,
  IN wfn_target_hw TINYTEXT,
  IN vulnerable BIT(1),
  IN opt_version_start TINYTEXT,
  IN opt_version_start_boundary ENUM('I','E'),
  IN opt_version_end TINYTEXT,
  IN opt_version_end_boundary ENUM('I','E'),
  OUT conjunct_id INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT ''
BEGIN
  -- Save WFN item if needed
  INSERT INTO `t_wfn_item` (
    `t_wfn_item`.`digest`,
    `t_wfn_item`.`part`,
    `t_wfn_item`.`vendor`,
    `t_wfn_item`.`product`,
    `t_wfn_item`.`version`,
    `t_wfn_item`.`update`,
    `t_wfn_item`.`edition`,
    `t_wfn_item`.`sw_edition`,
    `t_wfn_item`.`target_sw`,
    `t_wfn_item`.`target_hw`
  )
  VALUES (
    f_compute_digest(CONCAT(wfn_part,wfn_vendor,wfn_product,wfn_version,wfn_update,wfn_edition,wfn_sw_edition,wfn_target_sw,wfn_target_hw)),
    wfn_part,
    wfn_vendor,
    wfn_product,
    wfn_version,
    wfn_update,
    wfn_edition,
    wfn_sw_edition,
    wfn_target_sw,
    wfn_target_hw
  )
  ON DUPLICATE KEY UPDATE
    `t_wfn_item`.`id` = LAST_INSERT_ID(`t_wfn_item`.`id`);
  -- Retrieve last inserted wfn id
  SELECT LAST_INSERT_ID() INTO conjunct_id;
  -- Then save or CVE configuration conjunction
  INSERT INTO `t_cve_item_configuration_conjunct` (
    `t_cve_item_configuration_conjunct`.`cve`,
    `t_cve_item_configuration_conjunct`.`configuration`,
    `t_cve_item_configuration_conjunct`.`disjunct`,
    `t_cve_item_configuration_conjunct`.`wfn`,
    `t_cve_item_configuration_conjunct`.`vulnerable`,
    `t_cve_item_configuration_conjunct`.`opt_version_start`,
    `t_cve_item_configuration_conjunct`.`opt_version_start_boundary`,
    `t_cve_item_configuration_conjunct`.`opt_version_end`,
    `t_cve_item_configuration_conjunct`.`opt_version_end_boundary`
  )
  VALUES (
    cve,
    configuration,
    disjunct,
    conjunct_id,
    vulnerable,
    opt_version_start,
    opt_version_start_boundary,
    opt_version_end,
    opt_version_end_boundary
  );
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_cve_complete_update_process`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_complete_update_process`(
  IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Update a CVE generator entry and save the update process end date.'
BEGIN
  UPDATE `t_cve_generator`
    SET `t_cve_generator`.`process_end_date` = NOW()
    WHERE `t_cve_generator`.`id` = generator;
END//
DELIMITER ;

-- ----------------------------------------------------------------------------
-- Asset related procedures
-- ----------------------------------------------------------------------------
-- Procedure `{{DATA_DB_NAME}}`.`p_asset_start_update_process`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_asset_start_update_process`(
  IN server_name VARCHAR(64),
  IN server_version VARCHAR(16),
  IN agent_name VARCHAR(64),
  IN agent_version VARCHAR(16),
  OUT generator_id INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Create a new Asset generator entry and save its id into "generator_id".'
BEGIN
  INSERT INTO `t_asset_generator` (
    `t_asset_generator`.`process_start_date`,
    `t_asset_generator`.`server_name`,
    `t_asset_generator`.`server_version`,
    `t_asset_generator`.`agent_name`,
    `t_asset_generator`.`agent_version`
  )
  VALUES (
    NOW(),
    server_name,
    server_version,
    agent_name,
    agent_version
  );
  -- Retrieve last inserted generator id
  SELECT LAST_INSERT_ID() INTO generator_id;
END//
DELIMITER ;
-- Procedure `{{DATA_DB_NAME}}`.`p_asset_save_host`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_asset_save_host`(
  IN inet4_addr INT UNSIGNED,
  IN inet4_mask INT UNSIGNED,
  IN hostname TINYTEXT,
  IN generator INT UNSIGNED,
  OUT host_id INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save a new host or update existing.'
BEGIN
  INSERT INTO `t_host_item` (
    `t_host_item`.`inet4_addr`,
    `t_host_item`.`inet4_mask`,
    `t_host_item`.`hostname`,
    `t_host_item`.`generator`
  )
  VALUES (
    inet4_addr,
    inet4_mask,
    name,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_host_item`.`id` = LAST_INSERT_ID(`t_host_item`.`id`),
    `t_host_item`.`hostname` = hostname,
    `t_host_item`.`generator` = generator;
  -- Retrieve last inserted host id
  SELECT LAST_INSERT_ID() INTO host_id;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_asset_drop_host`

-- Procedure `{{DATA_DB_NAME}}`.`p_asset_save_asset`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_asset_save_asset`(
  IN host INT UNSIGNED,
  IN wfn_part ENUM('a','o','h'),
  IN wfn_vendor TINYTEXT,
  IN wfn_product TINYTEXT,
  IN wfn_version TINYTEXT,
  IN wfn_update TINYTEXT,
  IN wfn_edition TINYTEXT,
  IN wfn_sw_edition TINYTEXT,
  IN wfn_target_sw TINYTEXT,
  IN wfn_target_hw TINYTEXT,
  IN generator INT UNSIGNED,
  OUT asset_id INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Save a new asset for a host or update existing.'
BEGIN
  DECLARE wfn_id INT UNSIGNED;
  -- Save WFN item if needed
  INSERT INTO `t_wfn_item` (
    `t_wfn_item`.`digest`,
    `t_wfn_item`.`part`,
    `t_wfn_item`.`vendor`,
    `t_wfn_item`.`product`,
    `t_wfn_item`.`version`,
    `t_wfn_item`.`update`,
    `t_wfn_item`.`edition`,
    `t_wfn_item`.`sw_edition`,
    `t_wfn_item`.`target_sw`,
    `t_wfn_item`.`target_hw`
  )
  VALUES (
    f_compute_digest(CONCAT(wfn_part,wfn_vendor,wfn_product,wfn_version,wfn_update,wfn_edition,wfn_sw_edition,wfn_target_sw,wfn_target_hw)),
    wfn_part,
    wfn_vendor,
    wfn_product,
    wfn_version,
    wfn_update,
    wfn_edition,
    wfn_sw_edition,
    wfn_target_sw,
    wfn_target_hw
  )
  ON DUPLICATE KEY UPDATE
    `t_wfn_item`.`id` = LAST_INSERT_ID(`t_wfn_item`.`id`);
  -- Retrieve last inserted wfn id
  SELECT LAST_INSERT_ID() INTO wfn_id;
  -- Save Asset
  INSERT INTO `t_asset_item` (
    `t_asset_item`.`host`,
    `t_asset_item`.`wfn`,
    `t_asset_item`.`generator`
  )
  VALUES (
    host,
    wfn_id,
    generator
  )
  ON DUPLICATE KEY UPDATE
    `t_asset_item`.`id` = LAST_INSERT_ID(`t_asset_item`.`id`),
    `t_asset_item`.`generator` = generator;
  -- Retrieve last inserted asset id
  SELECT LAST_INSERT_ID() INTO asset_id;
END//
DELIMITER ;

-- Procedure `{{DATA_DB_NAME}}`.`p_asset_drop_asset`

-- Procedure `{{DATA_DB_NAME}}`.`p_asset_complete_update_process`
DELIMITER //
CREATE PROCEDURE `{{DATA_DB_NAME}}`.`p_asset_complete_update_process`(
  IN generator INT UNSIGNED
)
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT 'Update an Asset generator entry and save the update process end date.'
BEGIN
  UPDATE `t_asset_generator`
    SET `t_asset_generator`.`process_end_date` = NOW()
    WHERE `t_asset_generator`.`id` = generator;
END//
DELIMITER ;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- Triggers
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- Trigger  `{{DATA_DB_NAME}}`.`tr_assess_updated_cve`


-- Trigger  `{{DATA_DB_NAME}}`.`tr_assess_updated_asset`
DELIMITER //
CREATE TRIGGER `{{DATA_DB_NAME}}`.`tr_assess_updated_asset`
AFTER UPDATE ON `{{DATA_DB_NAME}}`.`t_asset_item`
FOR EACH ROW
BEGIN
  -- Working data
  DECLARE cve_id INT;
  DECLARE cve_configuration INT;
  DECLARE cve_disjunct INT;
  DECLARE cve_match BINARY;
  -- Cursors : foreach CVE configuration disjunct
  DECLARE done INT DEFAULT 0;
  DECLARE foreach_cve_dis CURSOR FOR SELECT `t_cve_item_configuration_disjunct`.`cve`, `t_cve_item_configuration_disjunct`.`configuration`, `t_cve_item_configuration_disjunct`.`id` FROM `{{DATA_DB_NAME}}`.`t_cve_item_configuration_disjunct`;
  DECLARE CONTINUE handler FOR SQLSTATE '02000' set done = 1;
  -- Foreach CVE configuration disjunct entry
  OPEN foreach_cve_dis;
    foreach_loop: REPEAT
      -- Leave foreach loop if there is no more entry
      IF done THEN
          LEAVE foreach_loop;
      END IF;

      -- Fetch next CVE configuration disjunct entry
      FETCH foreach_cve_dis INTO cve_id, cve_configuration, cve_disjunct;
      -- Compare CVE configuration conjunction against host configuration
      SET cve_match = f_is_cve_match_host(cve_id, cve_configuration, cve_disjunct, NEW.host);
      IF cve_match THEN
        -- Insert or update an assessment entry if needed
        -- TODO: compute CVSS v2 and v3 Score
        INSERT INTO `{{DATA_DB_NAME}}`.`t_assessment` (
          `asset`,
          `cve`,
          `timestamp`,
          `cvssv2_score`,
          `cvssv3_score`
        )
        VALUES (
          NEW.id,
          cve_id,
          NOW(),
          0.0,
          0.0
        )
        ON DUPLICATE KEY UPDATE
          `t_assessment`.`timestamp` = NOW();
      END IF;

      UNTIL done
    END REPEAT;
  CLOSE foreach_cve_dis;
END//
DELIMITER ;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- Privileges
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- ----------------------------------------------------------------------------
-- Product inventory related privileges
-- ----------------------------------------------------------------------------
-- Read/Write/Execute access to all cpe related tables
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cpe_item`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cpe_generator`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cpe_item_reference`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cpe_item_title`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;
-- Read/Write/Execute access to wfn table
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_wfn_item`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;
-- Execute access to helper functions
GRANT EXECUTE
  ON FUNCTION `{{DATA_DB_NAME}}`.`f_compute_digest`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;
-- Execute access ton all CPE related procedures
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_start_update_process`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_save_cpe`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_save_cpe_title`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_save_cpe_reference`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cpe_complete_update_process`
  TO `u_canvas_product_inventory`@`{{DATA_U_CANVAS_PRODUCT_INVENTORY_HOSTNAME}}`;

-- ----------------------------------------------------------------------------
-- Vulnerability inventory related privileges
-- ----------------------------------------------------------------------------
-- Read/Write/Execute access to all cve related tables
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cve_item`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cve_generator`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cve_item_reference`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cve_item_description`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cve_item_affect`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cve_item_configuration`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cve_item_configuration_disjunct`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cve_item_configuration_conjunct`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cve_item_impact_cvssv2`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_cve_item_impact_cvssv3`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
-- Read/Write/Execute access to wfn table
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_wfn_item`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
-- Execute access to helper functions
GRANT EXECUTE
  ON FUNCTION `{{DATA_DB_NAME}}`.`f_compute_digest`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
-- Execute access ton all CVE related procedures
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_start_update_process`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve_impact_cvssv2`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve_impact_cvssv3`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve_description`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve_reference`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
    ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_save_cve_affect`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_cve_complete_update_process`
  TO `u_canvas_vulnerability_inventory`@`{{DATA_U_CANVAS_VULNERABILITY_INVENTORY_HOSTNAME}}`;

-- ----------------------------------------------------------------------------
-- Asset inventory related privileges
-- ----------------------------------------------------------------------------
-- Read/Write/Execute access to all asset related tables
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_asset_item`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_asset_generator`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_asset_item_environment_cvssv2`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_asset_item_environment_cvssv3`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_host_item`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;
-- Read/Write/Execute access to wfn table
GRANT SELECT, INSERT, UPDATE, DELETE
  ON `{{DATA_DB_NAME}}`.`t_wfn_item`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;
-- Execute access to helper functions
GRANT EXECUTE
  ON FUNCTION `{{DATA_DB_NAME}}`.`f_compute_digest`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;
-- Execute access ton all Asset related procedures
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_asset_start_update_process`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_asset_save_host`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_asset_save_asset`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;
GRANT EXECUTE
  ON PROCEDURE `{{DATA_DB_NAME}}`.`p_asset_complete_update_process`
  TO `u_canvas_asset_inventory`@`{{DATA_U_CANVAS_ASSET_INVENTORY_HOSTNAME}}`;

-- ----------------------------------------------------------------------------
-- Dashboard related privileges
-- ----------------------------------------------------------------------------
-- Read access to all tables
GRANT SELECT
  ON `{{DATA_DB_NAME}}`.*
  TO`u_canvas_dashboard`@`{{DATA_U_CANVAS_DASHBOARD_HOSTNAME}}`;

FLUSH PRIVILEGES;
